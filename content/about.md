+++
title = "about me"
date = "2020-04-02"
aliases = ["about-me","about-lapis","contact"]
[ author ]
  name = "lapisangularis"
+++

In the web they call me **lapisangularis**, and I'm a **Software Enginner**. I'm doing some things over the internet. 

In real, my name is **Przemek**, living a comfy life in **Poland**. I do various of things - sports, music, science, basement or party - It's all my things. My job is mainly Software Engineering. I'm doing it for several years now, and I did it from development, through devops to project management and mentoring. I'm a self-employed Mercenary Engineer, you could say - in a one person company called **[cyberforged](https://cyberforged.io/)**. I'm a coder, hacker, guitar player, workout amateur, wannabe scientist. I love hiking, swimming, video games. I like keeping things simple. And I like thinking about things. A lot. 

You can probably know me from my activities through **socialmedia**. I'm active on multiple reddit boards and on twitter. Or, you could see me playing guitar on instagram. Maybe you know me from facebook groups, discord, or 4chan. I'm sometimes active on stackexchange, or contributing to Open Source projects on GitHub. Some people know me from oldschool IRC communities on freenode.

Who knows, maybe you even know me in person? Either way, feel free to **contact me**, if you need something - or just for chat. Sometimes I tend to be busy though, so my reply can take a while if It's not job related. You're free to take anything you need from here. And **thank you** for reading all of this! Enjoy.
